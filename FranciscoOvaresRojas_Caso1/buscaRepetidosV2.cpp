/**
 * Algoritmo para buscar y mostrar un mínimo de dos palabras y todas sus combinaciones en un Array de strings.
 * @author Francisco Javier Ovares Rojas.
 * @date 03/09/2021
 */

#include <iostream>
#include <conio.h>
#include <string>
#include <cstdlib>
#include <vector>
#include <stdio.h>
#include <sstream>

using namespace std;

#define ARRAY_SIZE 4

struct Oracion {
    string oracion;
    vector<string> words;
    int indiceOracion;
    Oracion *next;

    Oracion(int pIndice, string pOracion, vector<string> pPalabras){
        oracion = pOracion;
        words = pPalabras;
        indiceOracion = pIndice;
        next = NULL;
    }
} *primerOracion;

void insertarOracion(int pIndice, string pOracion, vector<string> pPalabras) {
  Oracion *newOration = new Oracion(pIndice, pOracion, pPalabras);

  newOration->next = primerOracion;
  primerOracion = newOration;
}

// Separa las cadenas de caractéres.
vector<string> split(string pTexto, char pSeparador) {
    // Declaraciones.
    vector<string> palabrasResultantes;
    string splittedWord;
    int posInitial = 0;
    int posFound = 0;

    while(posFound < pTexto.size() ){
        posFound = pTexto.find(pSeparador, posInitial);
        splittedWord = pTexto.substr(posInitial, posFound - posInitial);
        posInitial = posFound + 1;
        palabrasResultantes.push_back(splittedWord);
    }
    
    return palabrasResultantes;
}

// Crea y devuelve las combinaciones posibles.
string hacerCombinaciones(vector<string> pCoincidencias) {
    int tamano = pCoincidencias.size(), contador;
    string palabra1, palabra2;
    string combinaciones;

    for(int i = 0; i < tamano; i++) {
        contador = i+1;
        while(contador < tamano) {
            combinaciones += "   -> " + pCoincidencias[i]+", " + pCoincidencias[contador] + "\n";
            //cout<<""+listaUno[i]+" "+listaUno[contador]+"\n"<<endl;
            contador++;
        }
    } 

    return combinaciones;
}

// Muestra las palabras que coinciden en las oraciones.
void mostrarMatchedWords (vector<string> pCoincidences) {
    string combinations;
    combinations = hacerCombinaciones(pCoincidences);

    cout<<"----Posibles Combinaciones----\n"<<combinations;
}

// Imprime las coincidencias si existe más de una.
bool imprimirCoinciencias (vector<string> matchesWords) {
    bool masDeDos = false;
    if (matchesWords.size() > 1) {
        cout<<"\t---¡MATCH!---"<<endl;
        mostrarMatchedWords(matchesWords);
        masDeDos = true;
    }
    return masDeDos;
}

bool compararPlabras(vector<string> pPalabrasPrimerCadena, vector<string>pOracionDosSeparada){

    // Se declaran string de tipo vector para almacenar las palabras.
    vector<string> matchesWords;
    bool masDeDos = false;
    
    // Iterando palabras de pPrimerCadena
    cout<<endl;
    for(int i = 0; i < pPalabrasPrimerCadena.size(); i++){
        // Iterando  pSegundaOracion
        for (int j = 0; j < pOracionDosSeparada.size(); j++){
            // Si existe coincidencia.
            if (pPalabrasPrimerCadena[i] == pOracionDosSeparada[j]){
                // Se guarda la coincidencia(solo una).
                matchesWords.push_back(pPalabrasPrimerCadena[i]);
            }
        }
    }
    
    // Se imprimen las coincidencias si hay mas de 1.
    masDeDos = imprimirCoinciencias(matchesWords);
    return masDeDos;
}

void mostrarObjetos(int pIndice){
     // muestra los objetos
    for (Oracion *temp = primerOracion; temp != NULL; temp = temp->next) {
        if (temp->indiceOracion == pIndice){
            cout<<"Indice: "<<temp->indiceOracion;
            cout<<"\tOracion: "<<temp->oracion;
            cout<<endl;
        }
    }
}
vector<string> wordsSegunIndice(int pIndice){
    vector<string> words;

    for (Oracion *temp = primerOracion; temp != NULL; temp = temp->next) {
        if (temp->indiceOracion == pIndice){
            words = temp->words;
        }
    }
    return words;
}

void compararOraciones(string pCadena[], int pArraySize) {
    vector<string> mainWords, alterWords;
    bool masDeDos = false;
    // se crean objetos oraciones con su indice y palabras spliteadas.
    // se hace el split una unica vez por oración.
    for (int indice = 0; indice < pArraySize; indice++){
        insertarOracion(indice, pCadena[indice], split(pCadena[indice], ' '));
    }
    
    // recorre las oraciones/cadenas.
    // cada oracion/cadena se compara con c/u, mainCadena es la cadena a comparar. 
    for (int mainCadena = 0; mainCadena < pArraySize ; mainCadena++){
        // alterChain será el indice de las cadenas comparativas
        for (int alterChain = pArraySize; alterChain > mainCadena; alterChain--) {
            // Mientras que las oraciones no coincidan se comparan. Si el indice excede no entra acá.
            if (pCadena[mainCadena] != pCadena[alterChain] && alterChain < pArraySize) {
                mainWords = wordsSegunIndice(mainCadena);
                alterWords = wordsSegunIndice(alterChain);
                //se comparan las palabras de ambas oraciones.
                masDeDos = compararPlabras(mainWords, alterWords);
                if (masDeDos == true){
                    mostrarObjetos(mainCadena);
                    mostrarObjetos(alterChain);
                    masDeDos = false;
                }
            } 
        }
    }
}

int main() {
    
    string primerCadena[] = {"estoy escribiendo la tarea", "voy mañana a ver la tarea",
    "mañana voy en carro", "estoy en el carro escribiendo la poesía"};
    
    compararOraciones(primerCadena, ARRAY_SIZE);

    getch();
    return 0;     
}