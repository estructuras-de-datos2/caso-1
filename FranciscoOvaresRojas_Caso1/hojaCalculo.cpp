/**
 * Algoritmo de hoja de cálculo Exceel.
 * @author Francisco Javier Ovares Rojas.
 * @date 29/08/2021
 */
 
#include <iostream>
#include <conio.h>
#include <cstdlib>
#include <vector>

using namespace std;

#define CANTIDAD_MAXIMA_SUMAR  5
#define FILAS 10
#define COLUMNAS 10

struct Matriz {
    int matriz[FILAS][COLUMNAS];
}hojaCalculo;

struct Sumas {
    int fila;
    int columna;
    int value;
    bool isSuma = NULL;
    int valoresSumados[CANTIDAD_MAXIMA_SUMAR];
    Sumas* next;

    Sumas (int pFila, int pColum, vector<int> pSumados, bool pIsSum){
        fila = pFila;
        columna = pColum;
        isSuma = pIsSum;
        for (int i = 0; i < CANTIDAD_MAXIMA_SUMAR; i++) {
            valoresSumados[i] = pSumados[i];
        }
    }
}*primerSuma;

void insertarSuma(int pFila, int pColum, vector<int> pSum, bool isSum) {
  Sumas *newNode = new Sumas(pFila, pColum, pSum, isSum);

  newNode->next = primerSuma;
  primerSuma = newNode;
}

void sumarCeldas() {
    int fila, columna;
    int suma = 0, contadorSumas = 0, cantidadValores = 3;
    int valores[CANTIDAD_MAXIMA_SUMAR];
    vector<int> valoresYaSumados;
    bool isCero = false;
    // seleccion aleatoria del dato
    srand(time(NULL));
    fila = 0 + rand() % (9);
    columna = 0 + rand() % (9);
    // iterando sobre matriz
    for (int i = 0; i < CANTIDAD_MAXIMA_SUMAR; i++) {
        // cantidad de celdas a sumar = 2.
        contadorSumas = 0;
        suma = 0;
        while(contadorSumas < cantidadValores){
            fila = 0 + rand() % (9);
            columna = 0 + rand() % (9);
            // si el valor es 0 no quiero que se sume.
            if (hojaCalculo.matriz[fila][columna] == 0){
                continue;
            }
            else{
                valoresYaSumados.push_back(hojaCalculo.matriz[fila][columna]);
                suma += hojaCalculo.matriz[fila][columna];
                contadorSumas++;
                cout<<"\nSe sumó el valor "<<hojaCalculo.matriz[fila][columna]<<" fila ["<<fila<<"] columna ["<<columna<<"]";

                // guardando la suma en otra celda
                while (isCero == false && contadorSumas == cantidadValores){
                    // se busca celda vacía.
                    fila = 0 + rand() % (9);
                    columna = 0 + rand() % (9);
                    if (hojaCalculo.matriz[fila][columna] == 0){
                        hojaCalculo.matriz[fila][columna] = suma;
                        isCero = true;
                        cout<<"\nLa suma se guardó en la fila ["<<fila<<"] columna ["<<columna<<"]."<<endl;
                        insertarSuma(fila, columna, valoresYaSumados, true);
                    } 
                }
                isCero = false;
                
                
            }
        }
        cout<<endl;  
    }
}

void insertarValores() {
        
    int cant = 0;
    int pValue;
    int filaRand, columnaRand;
    bool isCero = false;
    cout<<"Cuántos valores desea ingresar: "; cin>>cant;

    for (int i = 0; i < cant; i++) {
        cout<<"Ingrese el valor "<<i<<": ";cin>>pValue;
        
        while (isCero == false){
            // se busca celda vacía.
            srand(time(NULL));
            filaRand = 0 + rand() % (9);
            columnaRand = 0 + rand() % (9);
            if (hojaCalculo.matriz[filaRand][columnaRand] == 0){
                cout<<"El valor se guardó en la fila ["<<filaRand<<"] y columna ["<<columnaRand<<"].\n";
                hojaCalculo.matriz[filaRand][columnaRand] = pValue;
                isCero = true;
            } 
        }
        isCero = false;

    }
}

void mostrarTablaEccel(){
    for (int i = 0; i < FILAS; i++){
        for (int j = 0; j < COLUMNAS; j++) {
            cout<<hojaCalculo.matriz[i][j]<<"    ";
        }
        cout<<"\n";
    }
}

int main() {

    insertarValores();
    cout<<"\n-------Hoja 1-------\n";
    mostrarTablaEccel();

    cout<<endl;
    
    sumarCeldas();
    cout<<"\n-------Hoja 2-------\n";
    mostrarTablaEccel();

    getch();
    return 0;
}