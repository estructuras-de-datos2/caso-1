/**
 * Algoritmo para buscar y mostrar un mínimo de dos palabras y todas sus combinaciones en un Array de strings.
 * @author Francisco Javier Ovares Rojas.
 * @date 27/08/2021
 */

#include <iostream>
#include <conio.h>
#include <string>
#include <cstdlib>
#include <vector>
#include <stdio.h>

using std::cout;
using std::cin;
using std::endl;
using std::string;
using std::vector;

#define ARRAY_SIZE 3

// Crea y devuelve las combinaciones posibles.
string hacerCombinaciones(vector<string> pCoincidencias) {
    int tamano = pCoincidencias.size(), contador;
    string palabra1, palabra2;
    string combinaciones;

    for(int i = 0; i < tamano; i++) {
        contador = i+1;
        while(contador < tamano) {
            combinaciones += "   -> " + pCoincidencias[i]+", " + pCoincidencias[contador] + "\n";
            //cout<<""+listaUno[i]+" "+listaUno[contador]+"\n"<<endl;
            contador++;
        }
    } 

    return combinaciones;
}

// Muestra las palabras que coinciden en las oraciones.
void mostrarMatchedWords (vector<string> pCoincidences) {
    string combinations;
    combinations = hacerCombinaciones(pCoincidences);

    cout<<"----Posibles Combinaciones----\n"<<combinations;
}

// Muestra las cadenas que coinciden.
void imprimirMatchesChains(string pMatches){
    // Se verifica si hay coincidencias almacenadas(si las hay se muestran).
    for(int i = 0; i < pMatches.size(); i++){
        cout<<pMatches[i];
    } 
    cout<<"\n";  
}

// Separa las cadenas de caractéres.
vector<string> split(string pTexto, char pSeparador) {
    // Declaraciones.
    vector<string> palabrasResultantes;
    string splittedWord;
    int posInitial = 0;
    int posFound = 0;

    while(posFound < pTexto.size() ){
        posFound = pTexto.find(pSeparador, posInitial);
        splittedWord = pTexto.substr(posInitial, posFound - posInitial);
        posInitial = posFound + 1;
        palabrasResultantes.push_back(splittedWord);
    }
    
    return palabrasResultantes;
}

// Imprime las coincidencias si existe más de una.
void imprimirCoinciencias (vector<string> matchesWords, string pPrimerCadena, string pSegundaOracion) {
    if (matchesWords.size() > 1) {
        imprimirMatchesChains(pPrimerCadena);
        imprimirMatchesChains(pSegundaOracion);
        cout<<"\t---¡MATCH!---"<<endl;
        mostrarMatchedWords(matchesWords);
    }
}

void compararPlabras(string pPrimerCadena, string pSegundaOracion){

    // Se declaran string de tipo vector para almacenar las palabras.
    vector<string> palabrasPrimerCadena, oracionDosSeparada, matchesWords;

    // Se separan las oraciones en palabras y se almacenan en arrays.
    palabrasPrimerCadena = split(pPrimerCadena, ' ');
    oracionDosSeparada = split(pSegundaOracion, ' ');
    
    // Iterando palabras de pPrimerCadena
    cout<<endl;
    for(int i = 0; i < palabrasPrimerCadena.size(); i++){
        // Iterando  pSegundaOracion
        for (int j = 0; j < oracionDosSeparada.size(); j++){
            // Si existe coincidencia.
            if (palabrasPrimerCadena[i] == oracionDosSeparada[j]){
                // Se guarda la coincidencia(solo una).
                matchesWords.push_back(palabrasPrimerCadena[i]);
            }
        }
    }
    
    // Se imprimen las coincidencias si hay mas de 1.
    imprimirCoinciencias(matchesWords, pPrimerCadena, pSegundaOracion);
}

void compararOraciones(string pCadena[], int pArraySize) {

    vector<string> matchFounded;
    string chainOne, secondChain;

    //recorre las oraciones/cadenas.
    for (int indice = 0; indice < pArraySize ; indice++){
        //se guarda la oración, luego tomará la segunda, asi sucesivamente.
        chainOne.assign(pCadena[indice]);
        for (int iteracionSecundaria = pArraySize; iteracionSecundaria > indice; iteracionSecundaria--) {
            // Mientras que las oraciones no coincidan se comparan.
            if (pCadena[indice] != pCadena[iteracionSecundaria]) {
                // Si el indice excede no entra acá.
                if (iteracionSecundaria <= pArraySize) {
                    // Se guarda la oración+1 mientras que no exceda el indice de pCadena.
                    secondChain.assign(pCadena[iteracionSecundaria]);
                }
            }
            //se comparan las palabras de ambas oraciones.
            compararPlabras(chainOne, secondChain);
        }
    }
}

int main() {
    
    string primerCadena[] = {"estoy escribiendo la tarea", "voy mañana a ver la tarea",
    "mañana voy en carro", "estoy en el carro escribiendo la poesía"};
    string segundaCadena[] = {"test arbol casa","arbol ayer temprano","arbol casa ayer temprano","ayer hola casa"};
    
    compararOraciones(primerCadena, ARRAY_SIZE);
    compararOraciones(segundaCadena, ARRAY_SIZE);

    getch();
    return 0;     
}